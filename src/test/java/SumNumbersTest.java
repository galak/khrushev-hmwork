import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.*;

public class SumNumbersTest {
    private SumNumbers sut = null;

    @Before
    public  void setUp(){
        sut = new SumNumbers();
    }

    @Test
    public void emptyFile() throws FileNotFoundException {
        int actual = sut.sumNumb("./src/test/resources/sum_numbers_empty.txt");

        assertEquals(0, actual);
    }

    @Test
    public void singleLine() throws FileNotFoundException {
        int actual = sut.sumNumb("./src/test/resources/sum_numbers_singleline.txt");

        assertEquals(42, actual);
    }

    @Test(expected = FileNotFoundException.class)
    public void nonExistingFile() throws IOException {
        File nonExistingFile = File.createTempFile("sumNumbers", "");
        assertTrue(nonExistingFile.delete());

        sut.sumNumb(nonExistingFile.getAbsolutePath());
    }
}
