import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class AdvancedSumNumbersTest {
    private SumNumbers sut = null;

    private final String filePath;

    private final int expected;

    public AdvancedSumNumbersTest(String filePath, int expected) {
        this.filePath = filePath;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static List<Object[]> positiveCases() {
        return Arrays.asList(
                new Object[]{"./src/test/resources/sum_numbers_empty.txt", 0},
                new Object[]{"./src/test/resources/sum_numbers_singleline.txt", 42}
        );
    }

    @Before
    public void setUp() {
        sut = new SumNumbers();
    }

    @Test
    public void positiveCase() throws FileNotFoundException {
        int actual = sut.sumNumb(filePath);

        assertEquals(expected, actual);
    }

    @Test(expected = FileNotFoundException.class)
    public void nonExistingFile() throws IOException {
        File nonExistingFile = File.createTempFile("sumNumbers", "");
        assertTrue(nonExistingFile.delete());

        sut.sumNumb(nonExistingFile.getAbsolutePath());
    }
}
