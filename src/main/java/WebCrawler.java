import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Администратор on 22.01.15.
 */
public class WebCrawler {
    public void RWSite(String urlName, String path){
        File fileOut = new File(path);
        List<String> content = new ArrayList<String>();
        try {
        URL url = new URL(urlName);
        URLConnection connection = url.openConnection();
            connection.connect();
            Scanner in = new Scanner(connection.getInputStream());
            while (in.hasNextLine()){
                content.add(in.nextLine());
            }
            in.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        RandomAccessFile raf = null;
        try {
            try {
                raf = new RandomAccessFile(fileOut, "rw");
                for ( String line : content){
                   raf.writeBytes(line);
                }
                raf.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (raf != null) {
                    raf.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
