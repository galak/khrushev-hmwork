import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by Администратор on 22.01.15.
 */
public class SumNumbers {
    public int sumNumb(String path) throws FileNotFoundException {
        int sum = 0;

        Pattern p = Pattern.compile("\\D+");
        Scanner in = new Scanner(new File(path));
        in.useDelimiter(p);

        while (in.hasNextInt()) {
            sum += in.nextInt();
        }
        in.close();

        return  sum;
    }
}
