import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Pattern;

public class Anagrams {
  private  Map<Map<Character, Integer>, List<String>> anagrams = new
          HashMap<Map<Character,Integer>, List<String>>();

    public void writeAnagram(String pathIn, String pathOut) {
        try{
        File fileOut = new File(pathOut);
        fileToMap(pathIn);
            PrintWriter out = new PrintWriter(fileOut);
            for (Map.Entry<Map<Character, Integer>, List<String>> entry : anagrams.entrySet()) {
                if (entry.getValue().size() > 1) {
                    out.println(entry.getValue());
                }
            }
            out.close();}
        catch (FileNotFoundException e){
            System.out.println("file not found");
        }
          }

    private void fileToMap(String pathIn) throws FileNotFoundException {
        File fileIn = new File(pathIn);
            Scanner in = new Scanner(fileIn);
            Pattern p = Pattern.compile("[ \\t\\x0B\\f\\r\\n\\.,\\-]");
            in.useDelimiter(p);
            while (in.hasNext()) {
                String word = in.next();
                addValue(word);
            }
            in.close();
       }

    private void addValue(String word) {
        String keyWord = word.toLowerCase();
        Map <Character, Integer> key = new HashMap<Character, Integer>();
        for (int j = 0; j < keyWord.length(); j++) {
            char symbol = keyWord.charAt(j);
                int count = 1;
                if (key.containsKey(symbol)) {
                    count = key.get(symbol) + 1;
                }
                key.put(symbol, count);
            }

        List aList;
        if (anagrams.containsKey(key)) {
            aList = anagrams.get(key);
        } else {
            aList = new ArrayList();
        }
        aList.add(word);
        anagrams.put(key, aList);
    }

}

