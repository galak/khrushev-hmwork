import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;
import java.util.regex.Pattern;

public class PseudoGraphic {
  private  Map<Pixel, String> surface = new HashMap<Pixel, String>();
    private int maxX = 0;
    private int maxY = 0;

    class Pixel {
        int x;
        int y;

        Pixel(int x, int y ) {
            this.x = x;
            this.y = y;
        }
    }
    private void fileToMap(String pathIn) throws FileNotFoundException{
        File fileIn = new File(pathIn);
        Scanner in = new Scanner(fileIn);
        Pattern p = Pattern.compile("\\W+");
        while (in.hasNextLine()) {
        Scanner scan = new Scanner(in.nextLine());
            scan.useDelimiter(p);
            int lt_x = scan.nextInt();
            int lt_y = scan.nextInt();
            int height = scan.nextInt();
            int weight = scan.nextInt();
            String symbol = scan.next();
            if (maxX < lt_x + weight){
                maxX = lt_x + weight;
            }
            if (maxY < lt_y + height){
                maxY = lt_y + height;
            }
            scan.close();
            for (int k = lt_y; k < lt_y + height; k++) {
                for (int j = lt_x; j < lt_x + weight; j++) {
                    surface.put(new Pixel(j, k), symbol);
                }
            }
        }
    }

    public void drawGraphic(String pathIn, String pathOut) {

        File fileOut = new File(pathOut);
        try {
        fileToMap(pathIn);

        RandomAccessFile file = new RandomAccessFile(fileOut, "rw");
        long pointer = 0;
        for (int n = 0; n < maxY ; n++) {
            file.seek(pointer + maxX);
            file.writeBytes("\n");
            pointer = file.getFilePointer();
        }
        for (Map.Entry<Pixel, String> entry : surface.entrySet()){
            int y = entry.getKey().y;
            int x = entry.getKey().x;
            String symbol = entry.getValue();
            file.seek(y*(maxX+1) + x);
            file.writeBytes(symbol);
        }
        file.close();
        }
        catch (FileNotFoundException e){
            System.out.println("file not found");
        }
        catch (IOException e){
            System.out.println("Negative seek offset");
        }
    }
}


